import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    axios_API_URL:"http://192.168.74.235:8000",
    StateMenuList:0,
    StateGridList:1,
    value_Increase_decrease:0,
  },
  mutations: {
    state_menu_list(state,n){
      state.StateMenuList = n
    },
    state_grid_list(state,n){
      state.StateGridList = n
    },
  },
  actions: {
  },
  modules: {
  }
})
