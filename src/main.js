import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import axios from 'axios'
import VueAxios from 'vue-axios'
import 'ant-design-vue/dist/antd.css';
import VueSweetalert2 from 'vue-sweetalert2';
import VAnimateCss from 'v-animate-css';
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import { DatePicker } from 'ant-design-vue';
import LongdoMap from 'longdo-map-vue' 
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import BootstrapVue from "bootstrap-vue";
import 'sweetalert2/dist/sweetalert2.min.css';
import Carousel3d from 'vue-carousel-3d';
import VueAgile from 'vue-agile'

Vue.use(VueAgile)
Vue.use(Carousel3d);
Vue.use(VueSweetalert2);
Vue.use(LongdoMap, {     
       load: {         
            apiKey: '2e03a7f66ce64b925612d903f6f1586e '     
       } 
})
Vue.use(BootstrapVue);
Vue.use(DatePicker);
Vue.use(VueAxios, axios)
Vue.use(Antd);
Vue.use(VAnimateCss);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')

// require styles
require("./assets/css/main.css");
require("./assets/fonts/stylesheet.css");

