import Vue from 'vue'
import VueRouter from 'vue-router'
import Homepage from '../views/Homepage.vue'
import Navbar from "../views/Navbar.vue";
import Test from "../views/text.vue";

// Shop
const Shop = () => import("@/components/Shop/Shop.vue");
const Produce = () => import("@/components/Shop/Produce.vue");
// Solutions
const Solutions = () => import("@/components/Solutions/Solutions.vue");
const SmartFarming = () => import("@/components/Solutions/Industrial_Solution/SmartFarming.vue");
// NexpieNews
const News = () => import("@/components/News/News.vue");
// AboutUS
const AboutUS = () => import("@/components/AboutUS/AboutUS.vue");


Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "Navbar",
    component: Navbar,
    children: [
      // home
      {
        path: "/Homepage",
        name: "Homepage",
        component: Homepage
      },
      // Shop
      {
        path: "/Shop",
        name: "Shop",
        component: Shop,
      },
      {
        path: "/Shop/Produce",
        name: "Produce",
        component: Produce
      },
      // solutions
      {
        path: "/Solutions",
        name: "Solutions",
        component: Solutions
      },
      {
        path: "/Solutions/SmartFarming",
        name: "SmartFarming",
        component: SmartFarming
      },
      // News
      {
        path: "/News",
        name: "News",
        component: News
      },
      // AboutUS
      {
        path: "/AboutUS",
        name: "AboutUS",
        component: AboutUS
      },

      //redirect
      {
        path: "/",
        name: "noPath",
        redirect: '/Homepage',
        component: Homepage
      },
      {
        path: "/*",
        name: "otherPath",
        redirect: '/Homepage',
        component: Homepage
      },
      { path: '/*', redirect: '/Homepage' },
    ]
  },

]

const router = new VueRouter({
  routes
})

export default router
